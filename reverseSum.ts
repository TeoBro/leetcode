function reverseSum(num): boolean {
  if(num == 0) {
      return true
  }
  let i = Math.floor(num/2)
  while(i<num) {

      if(i+reverseANumber(i) == num) {
        console.log(num, i)
          return true
      }
      i++
  }
  return false
};

function reverseANumber(x, base = 10) {
  console.log(x, "Начальный х")
	let result = 0;
  let lastDigit: number;
  
	while (x != 0) {
		lastDigit = x % base;
    console.log(lastDigit, 'Последняя цифра')
		result = result * base + lastDigit;
    console.log(result, 'Результат')
		x = Math.floor(x / base);
    console.log(x, 'Конечный х')
	}

	return result;
};

reverseSum(181);