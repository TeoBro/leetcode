/**
You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
 */

/**
 Do not return anything, modify matrix in-place instead.
 */
 function rotate(matrix: number[][]): void {
  transpose(matrix);
  reverse(matrix);
};

function transpose(matrix: number[][]) {
  let n = matrix.length;
  for (let i = 0; i < n; i++) {
      for (let j = i + 1; j < n; j++) {
          let template = matrix[j][i];
          matrix[j][i] = matrix[i][j];
          matrix[i][j] = template;
      }
  }
}

function reverse(matrix: number[][]): void {
  let n = matrix.length;
  for (let i = 0; i < n; i++) {
      for (let j = 0; j < n/2; j++){
          let template = matrix[i][j]
          matrix[i][j] = matrix[i][n - j - 1];
          matrix[i][n - j - 1] = template;
      }
  }
}

