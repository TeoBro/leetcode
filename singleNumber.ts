/**Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.**/


function singleNumber(nums: number[]): number {
  let answer = 0;
  if (nums.length === 1) answer = nums[0];
  
  for (let i = 0; i < nums.length; i++) {
     if(nums.lastIndexOf(nums[i]) === nums.indexOf(nums[i])) answer = nums[i];
  }
  
  return answer;
};