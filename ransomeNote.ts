function canConstruct(ransomNote: string, magazine: string): boolean {
  let map = new Map();
  let result = true
   magazine.split('').forEach((el) => {
       if (map.has(el)) {
           map.set(el, map.get(el) + 1)
       } else {
           map.set(el, 1)
       }
   })
   
   ransomNote.split('').forEach((el) => {
       if(map.has(el)) {
           if(map.get(el) - 1 == 0) {
               map.delete(el);
           } else {
               map.set(el, map.get(el) - 1)
           }
       } else result = false;
   })
   return result;
};