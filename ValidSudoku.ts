/**Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules. */

function isValidSudoku(board: string[][]): boolean {
  let mapRow = new Map();
  let mapColumn = new Map();
  let mapBlock1 = new Map();
  let mapBlock2 = new Map();
  let mapBlock3 = new Map();
  
  for( let  i = 0; i < board.length; i++) {
      for (let j = 0; j < board.length; j++) {
          
          // Логика для проверки строки
          if (mapRow.has(board[i][j])) {
              return false;
          } else if (board[i][j] !== '.') {
              mapRow.set(board[i][j], 1);
          }
          
          // Логика проверки блоков в рамках строки
          if (((i <= 2 || (i > 2 && i <= 5) || (i > 5 && i <= 8)) && j <= 2)) {
              if (mapBlock1.has(board[i][j])) {
                  return false
              } else if (board[i][j] !== '.') {
                  mapBlock1.set(board[i][j], 1)
              }
          }
          
          if ((i <= 2 || (i > 2 && i <= 5) || (i > 5 && i <= 8)) && (j > 2 && j <= 5)) {
              if (mapBlock2.has(board[i][j])) {
                  return false
              } else if (board[i][j] !== '.') {
                  mapBlock2.set(board[i][j], 1)
              }
          }
          
          if ((i <= 2 || (i > 2 && i <= 5) || (i > 5 && i <= 8)) && ( j > 5 && j <= 8)) {
              if (mapBlock3.has(board[i][j])) {
                  return false
              } else if (board[i][j] !== '.') {
                  mapBlock3.set(board[i][j], 1)
              }
          }
          
          
          // Логика для проверки столбца
          if (mapColumn.has(board[j][i])) {
              return false;
          } else if (board[j][i] !== '.') {
              mapColumn.set(board[j][i], 1);
          }
          
          // Очищаем блоки, строки, столбцы
          if (i === 2 || i === 5) {
             if (j === 2) mapBlock1.clear();
             if (j === 5) mapBlock2.clear();
             if (j === 8) mapBlock3.clear(); 
          }
          
          if (j === board.length - 1) {
              mapRow.clear();
              mapColumn.clear(); 
          }
      }
  }
  return true;
};