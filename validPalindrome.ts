/**
 * A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters,
 * it reads the same forward and backward. Alphanumeric characters include letters and numbers.

Given a string s, return true if it is a palindrome, or false otherwise
 */

function isPalindrome(s: string): boolean {
  s = s.replace(/[^0-9a-zа-яё]/gi, '').toLowerCase();
  console.log(s)
  for (let i = 0; i < s.length / 2; i++) {
      if(s[i] === s[s.length - i - 1]) {
          continue;
      }
      return false;
  }
  return true;
};