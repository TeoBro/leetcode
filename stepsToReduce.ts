function numberOfSteps(num: number): number {
  let steps = 0;
  while(num) {
      num = num % 2 === 0 ? num / 2 : --num;
      steps++
  }

  return steps;
};