function maxProfit(prices: number[]): number {
  let buy: number = 0;
  let buyFlag = false
  let sell: number = 0;
  let profit = 0
  for (let i = 0; i < prices.length; i++) {
      if (prices[i] >= prices[i + 1] && !buyFlag) {
          continue
      } else if (prices[i] < prices[i + 1] && !buyFlag) {
          buy = prices[i];
          buyFlag = true
      } else if ((prices[i] > prices[i + 1] || prices[i + 1] === undefined) && buyFlag) {
          sell = prices[i];
          profit = profit + sell - buy;
          sell = 0;
          buy = 0;
          buyFlag = false;  
      }
  }
  
  return profit;
};