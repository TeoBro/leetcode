function maximumWealth(accounts: number[][]): number {
    let maxWealth = 0;
    accounts.map((account)=> {
      let sum = account.reduce((acc, currentValue) =>  acc + currentValue, 0)
      maxWealth = sum >= maxWealth ? sum : maxWealth;
    })
        
    return this.maxWelth;
};

