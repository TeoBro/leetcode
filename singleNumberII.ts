function singleNumber(nums: number[]): number {
  let ones = 0;
  let twos = 0;

  for (let num of nums) {
    
    ones = (ones ^ num) & ~twos;
    
    twos = (twos ^ num) & ~ones;
    console.log(ones, twos);
  }

  return ones;
}

singleNumber([1,2,3,3,4,5,1,5])