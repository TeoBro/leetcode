class ListNode {
  val: number
  next: ListNode | null
  constructor(val?: number, next?: ListNode | null) {
      this.val = (val===undefined ? 0 : val)
      this.next = (next===undefined ? null : next)
  }
}

function middleNode(head: ListNode | null): ListNode | null {
  let template = head;
  let numEl = 0;

  while(template) {
      template = template.next
      numEl++;
  }
  
  numEl = Math.floor(numEl / 2);
  while (numEl) {
          head = head && head.next
          numEl--;
  }

  return head;
};